(function (win, doc) {

    var button = jQuery('.cn-button'),
        wrapper = jQuery('.cn-wrapper');

    var open = false;

    function handler() {
        if (!open) {
            this.innerHTML = "Close";
            jQuery('.cn-wrapper').addClass('opened-nav');
        }
        else {
            this.innerHTML = "Menu";
            jQuery('.cn-wrapper').removeClass('opened-nav');
        }
        open = !open;
    }

    function closeWrapper() {
        jQuery('.cn-wrapper').removeClass('opened-nav');
    }

    jQuery(doc).ready(function () {
        jQuery('.cn-button').on('click', function () {
            if (!open) {
                this.innerHTML = "Close";
                jQuery('.cn-wrapper').addClass('opened-nav');
            }
            else {
                this.innerHTML = "Menu";
                jQuery('.cn-wrapper').removeClass('opened-nav');
            }
            open = !open;
        })
    })

})(window, document);
