(function (win, doc) {

    var gmapsUrl = 'https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1366.0533370334813!2d23.61227188365443!3d46.78250234739235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e0!4m0!4m3!3m2!1d46.7824829!2d23.6130133!5e0!3m2!1sen!2sro!4v1494053062100';

    /**
     * Function for  resizing the given container
     * @param variable - container variable
     * @param setHeight - boolean
     * @param setWidth - boolean
     */
    function containerResize(variable, setHeight, setWidth) {
        var viewportHeight = win.innerHeight,
            viewportWidth = win.innerWidth,
            setHeight = setHeight || false,
            setWidth = setWidth || false,
            height = viewportHeight + 'px',
            width = viewportWidth + 'px';

        if (setWidth) {
            variable.css('width', width);
        } else if (setHeight) {
            variable.css('height', height);
        }
    }

    jQuery(doc).ready(function () {
        jQuery('.navigation-list li').on('click', function () {
            jQuery('.navigation-list li').removeClass('active');
            jQuery(this).toggleClass('active')
        });

        jQuery('.info-item').on('click', function () {
            // jQuery('.info-body').hide();
            jQuery(this).children('.info-body').toggle();
        });
        jQuery('.service').on('click', function () {
            // jQuery(this).find('.modal-view').css('display', 'inline-block');
            jQuery(this).find('.modal-view').toggleClass('current');
        });
    })

    jQuery(win).load(function () {
        jQuery('.google-maps').attr('src', gmapsUrl);
    })
})(window, document);
